// alert("hi");

function displayMsgToSelf(){
	console.log("Don't text him back");
};

// invoke 10 times
// send your console output screenshot in hangouts 

displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();

// While Loop
/*
    - A while loop takes in an expression/condition
    - Expressions are any unit of code that can be evaluated to a value
    - If the condition evaluates to true, the statements inside the code block will be executed
    - A statement is a command that the programmer gives to the computer
    - A loop will iterate a certain number of times until an expression/condition is met
    - "Iteration" is the term given to the repetition of statements
    - Syntax
        while(expression/condition) {
            statement
        }
*/

let count = 20;

// While the value of count is not equal to 0
while(count !== 0){
	// The message will be printed out
	displayMsgToSelf();

	// Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
	// Loops occupy a significant amount of memory space in our devices
	// Make sure that expressions/conditions in loops have their corresponding increment/decrement operators to stop the loop
	// Forgetting to include this in loops will make our applications run an infinite loop which will eventually crash our devices
	// After running the script, if a slow response from the browser is experienced or an infinite loop is seen in the console quickly close the application/browser/tab to avoid this
	count--;
};

/*Mini-activity: 5 mins.
	The following while loop should display the numbers from 1-5 (in order);

	Send your outputs in Hangouts
*/

// S O L U T I O N
let x = 1

while(x <= 5){
	console.log(x);
	x++;
};

// Do-While Loop
/*
    - A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.
    - Syntax
        do {
            statement
        } while (expression/condition)
*/

let doWhileCounter = 1;

do {
	console.log(doWhileCounter)

} while(doWhileCounter === 0)


/*
    - The "Number" function works similarly to the "parseInt" function
    - Both differ significantly in terms of the processes they undertake in converting information into a number data type and other features that help with manipulating data
    - The "prompt" function creates a pop-up message in the browser that can be used to gather user input
    - How the Do While Loop works:
        1. The statements in the "do" block executes once
        2. The message "Do While: " + number will be printed out in the console
        3. After executing once, the while statement will evaluate whether to run the next iteration of the loop based on given expression/condition (e.g. number less than 10)
        4. If the expression/condition is not true, another iteration of the loop will be executed and will be repeated until the condition is met
        5. If the expression/condition is true, the loop will stop
*/
let number = Number(prompt("Give me a number"));

do {

	// The current value of number is printed out
	console.log("Do While: " + number)

	// Increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
	// number = number + 1
	number += 1

	// Providing a number of 10 or greater will run the code block once and will stop the loop
} while(number < 10);

/*
	Mini-Activity: 5 mins
	Create a do-while loop which will be able to show the numbers in the console from 1-20 in order

	Send your outputs in Hangouts
*/

let counter = 1

// S O L U T I O N

do {
	console.log("Counter: " + counter)
	counter++
	//counter += 1

} while(counter <= 20)

// For Loop
/*
    - A for loop is more flexible than while and do-while loops. It consists of three parts:
        1. The "initialization" value that will track the progression of the loop.
        2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
        3. The "finalExpression" indicates how to advance the loop.
    - Syntax
        for (initialization; expression/condition; finalExpression) {
            statement
        }
*/

/*
    - Will create a loop that will start from 0 and end at 10
    - Every iteration of the loop, the value of count will be checked if it is equal or less than 10
    - If the value of count is less than or equal to 10 the statement inside of the loop will execute
    - The value of count will be incremented by one for each iteration
*/
for(let count = 0; count <= 10; count++){

	// The current value of count is printed out
	console.log("For Loop count: " + count);
};
// Individual characters of a string may be accessed using it's index number
// The first character in a string corresponds to the number 0, the next is 1 up to the nth number
let name = "AlEx";
// A === [0]
// l === [1]
// E === [2]
// x === [3]


// Characters in strings may be counted using the .length property

// Strings are special compared to other data types in that it has access to functions and other pieces of information another primitive data type might not have
console.log(name.length);

// Will create a loop that will print out the individual letters of the name variable
for(let i = 0; i < name.length; i++){

	// If the character of your name is a vowel letter, instead of displaying the character, display number "3"
	// The ".toLowerCase()" function/method will change the current letter being evaluated in a loop to a lowercase letter ensuring that the letters provided in the expressions below will match
	if(
		name[i].toLowerCase() == "a" ||
		name[i].toLowerCase() == "e" ||
		name[i].toLowerCase() == "i" ||
		name[i].toLowerCase() == "o" ||
		name[i].toLowerCase() == "u"
	){

		// If the letter in the name is a vowel, it will print the number 3
		console.log(3);

	} else {

		// Print in the console all non-vowel characters in the name
		console.log(name[i])
	}

};

// Continue and Break
/*
    - The "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
    - The "break" statement is used to terminate the current loop once a match has been found
*/

/*
    - Creates a loop that if the count value is divided by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
    - How this For Loop works:
        1. The loop will start at 0 for the the value of "count".
        2. It will check if "count" is less than the or equal to 20.
        3. The "if" statement will check if the remainder of the value of "count" divided by 2 is equal to 0 (e.g 0/2).
        4. If the expression/condition of the "if" statement is "true" the loop will continue to the next iteration.
        5. If the value of count is not equal to 0, the console will print the value of "count".
        6. The second if statement will check if the value of "count" is greater than 10. (e.g. 0)
        7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
        8. The value of "count" will be incremented by 1 (e.g. count = 1)
        9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement (e.g. name[0] > 10) is true, the loop will stop due to the "break" statement
*/

for(let count = 0; count <= 20; count++){

	// if remainder is equal to 0
	if(count % 2 === 0){

		// Tells the code to continue to the next iteration of the loop
		// This ignores all statements located after the continue statement;
		continue;
	};

	// The current value of number is printed out if the remainder is not equal to 0
	console.log("Continue and Break: " + count);

	// If the current value of count is greater than 10
	if(count > 10){

		// Tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
		// number values after 10 will no longer be printed
		break;
	};
};

let myName = "alexandro";

/*
    - Creates a loop that will iterate based on the length of the string
    - How this For Loop works:
        1. The loop will start at 0 for the the value of "i"
        2. It will check if "i" is less than the length of name (e.g. 0)
        3. The if statement will check if the value of name[i] converted to a lowercase letter a (e.g. name[0] = a)
        4. If the expression/condition of the if statement is true the loop will continue to the next iteration.
        5. If the value of name[i] is not equal to a, the second if statement will be evaluated
        6. The second if statement will check if the value of name[i] converted to a lowercase letter d (e.g. name[0] = d)
        7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
        8. The value of "i" will be incremented by 1 (e.g. i = 1)
        9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement (e.g. name[0] = d) is true, the loop will stop due to the "break" statement
*/

for(let i = 0; i < myName.length; i++){


	// The current letter is printed out based on it's index
	console.log(myName[i]);

	
	// If the vowel is equal to a, continue to the next iteration of the loop
	if(myName[i].toLowerCase() === "a"){

		console.log("Continue to next iteration")

		continue; // ignores all the remaining codes below once condition met, then will continue to the next iteration
	};

	// If the current letter is equal to d, stop the loop
	if(myName[i].toLowerCase() === "d"){

		break; // get out of the loops once condition met
	};
};
